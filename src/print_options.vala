/**
 * Class for storing (user-defined) print options
 *
 * When printing, the largest possible size between font_size_max and font_size_min is used.
 *
 * font must be a description of a monospace font.
 */
class PrintOptions : Object {
	/**
	 * Spacing between columns in px
	 */
	public double column_spacing { get; set; default = 10; }

	/**
	 * Font description
	 *
	 * As supported by [pango_font_description_from_string](https://developer.gnome.org/pango/stable/pango-Fonts.html#pango-font-description-from-string).
	 *
	 * Note that this must be a monospace font.
	 */
	public string font { get; set; default = "monospace"; }

	/**
	 * Minimum font size in px
	 */
	public int font_size_min { get; set; default = 9; }

	/**
	 * Maximum font size in px
	 */
	public int font_size_max { get; set; default = 18; }
}
