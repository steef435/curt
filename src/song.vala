/**
 * Parsing errordomain thrown by {@link Song.new_from_data}
 */
errordomain ParseError {
	FRONT_MATTER,
	MARKUP,
	SYNTAX,
	UNEXPECTED
}

/**
 * Printing errordomain thrown by {@link Song.print}
 */
errordomain PrintError {
	FIT,
	FONT
}

/**
 * Object representing a song paragraph
 */
class Paragraph : Object {
	public string[] line_sets { get; set; default = {}; }
}

/**
 * Object representing a song
 */
class Song : Object {
	/**
	 * Title of song
	 */
	public string title { get; set; }

	/**
	 * Artist of song
	 */
	public string artist { get; set; }

	/**
	 * Capo level (or 0)
	 */
	public uint capo { get; set; default = 0; }

	/**
	 * Array of paragraphs
	 *
	 * Markdown tags included.
	 */
	public Paragraph[] paragraphs { get; set; }

	/**
	 * Create a song object from text data
	 */
	public static Song new_from_data(string data) throws ParseError {
		Song song;
		string contents;
		string[] elements;
		Paragraph[] paragraphs;

		elements = /^---$/m.split(data);
		if (elements.length < 3) {
			throw new ParseError.SYNTAX("No front matter found. Expected: one. Did you forget a '---'?");
		} else if (elements.length > 3) {
			throw new ParseError.SYNTAX("More than one front matter found. Expected: one.");
		}
		if (elements[0] != "") {
			throw new ParseError.SYNTAX("Found '%s' at start of the file. Song files should start with '---'.", elements[0]);
		}

		/* Parse front matter */
		try {
			song = Json.gobject_from_data(typeof(Song), elements[1]) as Song;
		} catch (Error e) {
			throw new ParseError.FRONT_MATTER("Error parsing front matter JSON: " + e.message);
		}

		/* Title is required */
		if (song.title == null) {
			throw new ParseError.FRONT_MATTER("No title found. Setting a title is required.");
		}

		/* Parse contents */
		contents = elements[2];

		/* Remove useless white space */
		try {
			/* Remove leading empty lines */
			contents = /^\n+/.replace(contents, contents.length, 0, "");

			/* Remove trailing white space */
			contents._chomp();
		} catch (RegexError e) {
			throw new ParseError.UNEXPECTED("Unexpected regex error occurred: " + e.message);
		}

		/* Check for markup errors */
		try {
			MatchInfo match_info;

			/* Multiline markup is not supported */
			if (/<[^>\n]*>[^<\/]*\n+[^<\/]*<\/[^>\n]*>/.match(contents, 0, out match_info)) {
				while (match_info.matches()) {
					throw new ParseError.MARKUP(match_info.expand_references("Multiline markup is unsupported. Found here:\n '\\0'"));
				}
			}
		} catch (RegexError e) {
			throw new ParseError.UNEXPECTED("Unexpected regex error occurred: " + e.message);
		}

		/* Parse custom markup */
		try {
			/* <w>: Underline non-whitespace (words) only */
			RegexError? error = null;

			while (/<w>.*<\/w>/.match(contents)) {
				contents = /<w>(.*)<\/w>/i.replace_eval(contents, contents.length, 0, 0, (match_info, result) => {
					string match = match_info.fetch(1);

					/* Match found, enclose all words in <u></u> */
					try {
						result.append(/\S+/.replace(match, match.length, 0, "<u>\\0</u>"));
					} catch (RegexError e) {
						error = e;
					}

					return false;
				});

				if (error != null) {
					throw error;
				}
			}
		} catch (RegexError e) {
			throw new ParseError.UNEXPECTED("Unexpected custom markup parsing error occurred: " + e.message);
		}

		/* Check Pango parsing */
		try {
			Pango.parse_markup(contents, contents.length, 0, null, null, null);
		} catch (Error e) {
			throw new ParseError.MARKUP("Error parsing Pango markup: " + e.message);
		}

		/* Parse paragraphs and line sets */
		paragraphs = {};
		elements = /\n*===+\n*/.split(contents);
		foreach (string p in elements) {
			var paragraph = new Paragraph();
			paragraph.line_sets = /\n\n+/.split(p);
			paragraphs += paragraph;
		}

		song.paragraphs = paragraphs;

		return song;
	}

	/**
	 * Return a string representation of the song.
	 *
	 * Markup tags aren't parsed.
	 */
	public string to_string() {
		StringBuilder builder;

		builder = new StringBuilder();

		for (var pi = 0; pi < paragraphs.length; pi++) {
			for (var li = 0; li < paragraphs[pi].line_sets.length; li++) {
				builder.append(paragraphs[pi].line_sets[li]);

				/* Add \n if not the last */
				if (li + 1 < paragraphs[pi].line_sets.length) {
					builder.append("\n");
				}
			}

			/* Add \n\n if not the last */
			if (pi + 1 < paragraphs.length) {
				builder.append("\n\n");
			}
		}

		return builder.str;
	}

	/**
	 * Build layouts list for printing
	 *
	 * Returns an array of Pango.Layout. Front matter forms one layout. Each line set forms a Layout. Additionally, paragraphs are separated using empty layouts (effectively acting as a trailing \n)
	 */
	Pango.Layout[] build_layouts(Gtk.PrintContext print_context) {
		Pango.Layout[] layouts;
		Pango.Layout front_matter;
		Pango.Layout separator;
		string front_matter_str;

		layouts = { };

		/* Front matter */
		front_matter_str = "";
		if (title != null) {
			front_matter_str += @"<b>$title</b>\n";
		}
		if (artist != null) {
			front_matter_str += @"$artist\n";
		}
		if (capo != 0) {
			front_matter_str += @"<i>Capo: $capo</i>\n";
		}

		front_matter = print_context.create_pango_layout();
		front_matter.set_markup(front_matter_str._strip(), -1);
		layouts += front_matter;

		/* Separator */
		separator = print_context.create_pango_layout();
		separator.set_text("", -1);
		layouts += separator;

		/* Line sets */
		for (var i = 0; i < paragraphs.length; i++) {
			/* Create layouts for line sets */
			foreach (var line_set in paragraphs[i].line_sets) {
				var layout = print_context.create_pango_layout();
				layout.set_markup(line_set, -1);
				layouts += layout;
			}

			/* Add paragraph break if not the last */
			if (i + 1 < paragraphs.length) {
				var layout = print_context.create_pango_layout();
				layout.set_text("", -1);
				layouts += layout;
			}
		}

		return layouts;
	}

	/**
	 * Returns whether FontDescription font describes a monospace font
	 *
	 * Throws PrintError.FONT if the family isn't found in the Pango.FontMap
	 */
	bool font_family_is_monospace(string family_str, Pango.FontMap font_map) throws PrintError {
		/* Yes, this is an array of *pointers*:
		 * http://www.valadoc.org/#!api=pango/Pango.Context.list_families
		 * https://developer.gnome.org/pango/stable/pango-Fonts.html#pango-font-map-list-families */
		Pango.FontFamily*[] families;

		font_map.list_families(out families);

		foreach (var family in families) {
			if (family->get_name().casefold() == family_str.casefold()) {
				return family->is_monospace();
			}
		}

		/* Family not found */
		throw new PrintError.FONT(@"Font family '$family_str' not found.");
	}

	/**
	 * Returns whether an array of layouts fit within the given bounds
	 *
	 * - Layouts are placed directly beneath each-other.
	 * - width and height are in device units (pixels)
	 */
	bool layouts_fit(Pango.Layout[] layouts, double width, double height) {
		double total_height;

		total_height = 0;
		foreach (var layout in layouts) {
			int layout_width, layout_height;

			layout.get_pixel_size(out layout_width, out layout_height);

			if (layout_width > width) {
				return false;
			}

			total_height += layout_height;
			if (total_height > height) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Draws an array of layouts from top to bottom, starting at index 0
	 */
	void draw_column(Cairo.Context context, Pango.Layout[] column) {
		int total_height;

		total_height = 0;

		foreach (var layout in column) {
			int height;

			context.move_to(0, total_height);
			Pango.cairo_show_layout(context, layout);

			layout.get_pixel_size(null, out height);
			total_height += height;
		}
	}

	/**
	 * Draw song on a {@link Gtk.PrintContext}
	 */
	public void print(Gtk.PrintContext print_context, PrintOptions options) throws PrintError {
		double column_width, column_height;
		Cairo.Context context;
		Pango.FontDescription font;
		Pango.Layout[] layouts;
		unowned Pango.Layout[] col1;
		unowned Pango.Layout[] col2;

		/* Build layouts */
		layouts = build_layouts(print_context);

		/* Font family, make sure it's monospace */
		font = Pango.FontDescription.from_string(options.font);
		if (font_family_is_monospace(font.get_family(), print_context.get_pango_fontmap()) == false) {
			throw new PrintError.FONT(@"The font $(options.font) does not describe a monospace font. The font must be monospace.");
		}

		/* Calculate column dimensions */
		column_width = print_context.get_width() / 2 - options.column_spacing / 2;
		column_height = print_context.get_height();

		/* Try font sizes */
		col1 = null;
		col2 = null;
		for (int size = options.font_size_max; size >= options.font_size_min; size--) {
			/* Apply font with size */
			font.set_size(size * Pango.SCALE);
			foreach (var layout in layouts) {
				layout.set_font_description(font);
			}

			/* Divide into columns */
			for (var i = layouts.length; i >= 0; i--) {
				if (layouts_fit(layouts[0:i], column_width, column_height)) {
					col1 = layouts[0:i];

					if (i < layouts.length) {
						if (layouts_fit(layouts[i:layouts.length], column_width, column_height)) {
							col2 = layouts[i:layouts.length];
						}
					} else {
						col2 = layouts[0:0];
					}

					break;
				}
			}

			/* Check if division succeeded */
			if (col2 != null) {
				break;
			}
		}

		/* Check if it fits */
		if (col1 == null) {
			throw new PrintError.FIT(@"The song does not fit on the page with a minimum font size of $(options.font_size_min) (failed on first column)");
		} else if (col2 == null) {
			throw new PrintError.FIT(@"The song does not fit on the page with a minimum font size of $(options.font_size_min) (failed on second column)");
		}

		/* It does fit, draw */
		context = print_context.get_cairo_context();

		/* Left column */
		draw_column(context, col1);

		/* Right column */
		context.translate(print_context.get_width() / 2 + options.column_spacing / 2, 0);
		draw_column(context, col2);

		context.set_source_rgb(0, 0, 0);
		context.fill();
	}
}
