class Main : Object {
	/* Command line parsing variables */
	static bool version = false;
	static bool print = false;
	static string? print_to_file = null;
	static bool print_to_stdout = false;
	static bool print_dialog = false;
	static string? printer = null;
	[CCode (array_length = false, array_null_terminated = true)]
	static string[]? files = null;
	static double column_spacing = -1;
	static string? font = null;
	static int font_size_min = 0;
	static int font_size_max = 0;
	static string? page_size = null;
	static bool page_landscape = false;
	static double margin_bottom = -1;
	static double margin_left = -1;
	static double margin_right = -1;
	static double margin_top = -1;

	/* Command line parsing options */
	const OptionEntry[] OPTIONS = {
		{ "version", 'v', 0, OptionArg.NONE, ref version, "print version and exit", null },
		{ "print", 'p', 0, OptionArg.NONE, ref print, "print song(s) to printer", null },
		{ "print-to-file", 'F', 0, OptionArg.FILENAME, ref print_to_file, "print song(s) to PDF file", "FILE" },
		{ "print-to-stdout", 's', 0, OptionArg.NONE, ref print_to_stdout, "print song(s) as text to standard output buffer", null },
		{ "print-dialog", 'd', 0, OptionArg.NONE, ref print_dialog, "show print dialog", null },
		{ "printer", 'P', 0, OptionArg.STRING, ref printer, "printer to use for printing (default: system default)", null },
		{ "column-spacing", 'c', 0, OptionArg.DOUBLE, ref column_spacing, "column spacing to use for printing (default: 10)", "SPACING" },
		{ "font", 'f', 0, OptionArg.STRING, ref font, "monospace font to use for printing (default: monospace)", "FONT" },
		{ "font-size-min", 'm', 0, OptionArg.INT, ref font_size_min, "minimal font size to use for printing (default: 9)", "SIZE" },
		{ "font-size-max", 'M', 0, OptionArg.INT, ref font_size_max, "maximal font size to use for printing (default: 18)", "SIZE" },
		{ "page-size", 'S', 0, OptionArg.STRING, ref page_size, "PWG 5101.1-2002 paper size to use for printing (default: iso_a4)", "SIZE" },
		{ "page-landscape", 'L', 0, OptionArg.NONE , ref page_landscape, "set printing paper orientation to landscape instead of portrait", null },
		{ "margin-bottom", 0, 0, OptionArg.DOUBLE, ref margin_bottom, "override bottom margin of page-size in mm (double precision)", "MARGIN" },
		{ "margin-left", 0, 0, OptionArg.DOUBLE, ref margin_left, "override left margin of page-size in mm (double precision)", "MARGIN" },
		{ "margin-right", 0, 0, OptionArg.DOUBLE, ref margin_right, "override right margin of page-size in mm (double precision)", "MARGIN" },
		{ "margin-top", 0, 0, OptionArg.DOUBLE, ref margin_top, "override top margin of page-size in mm (double precision)", "MARGIN" },
		{ "", 0, 0, OptionArg.FILENAME_ARRAY, ref files, null, "SONG..." },
		{ null }
	};

	/**
	 * Error count. Should be incremented whenever an error occurs.
	 */
	static uint error_count = 0;

	/**
	 * List of parsed Songs
	 */
	static Song[] songs = { };

	/**
	 * Create print operation for songs, using page_setup, print_options and print_settings
	 */
	static Gtk.PrintOperation create_print_operation(Gtk.PageSetup page_setup, PrintOptions print_options, Gtk.PrintSettings print_settings) {
		Gtk.PrintOperation print_operation;

		print_operation = new Gtk.PrintOperation();
		print_operation.default_page_setup = page_setup;
		print_operation.print_settings = print_settings;

		print_operation.embed_page_setup = true;
		print_operation.n_pages = songs.length;

		print_operation.draw_page.connect((context, number) => {
			try {
				songs[number].print(context, print_options);
			} catch (PrintError e) {
				stderr.printf("error printing %s: %s\n", songs[number].title, e.message);
				print_operation.cancel();
				error_count++;
			}
		});

		return print_operation;
	}

	/**
	 * Parse command line arguments
	 */
	static void parse_command_line_arguments(string[] args) throws OptionError {
		OptionContext context;

		context = new OptionContext(" - print song files");

		context.add_main_entries(OPTIONS, null);
		context.add_group(Gtk.get_option_group(false));

		context.parse(ref args);
	}

	static int main(string[] args) {
		/* Parse command line arguments */
		try {
			parse_command_line_arguments(args);
		} catch (OptionError e) {
			stderr.printf("error: %s\n", e.message);
			stderr.printf("Run '%s --help' to see a full list of available command line options.\n", args[0]);
			return 1;
		}

		/* Init Gtk if necessary */
		if (print_dialog) {
			Gtk.init(ref args);
		}

		/* Defaults for unspecified command line argument strings */
		if (page_size == null) {
			page_size = "iso_a4";
		}

		if (version) {
			stdout.printf("curt %s\n", VERSION);
			return 0;
		}

		if (files == null || files.length < 1) {
			stderr.printf("error: no files specified.\n");
			return 1;
		}

		/* Read songs */
		foreach (var file in files) {
			try {
				Song song;
				string contents;

				FileUtils.get_contents(file, out contents, null);

				song = Song.new_from_data(contents);

				songs += song;
			} catch (FileError e) {
				stderr.printf("error reading %s: %s\n", file, e.message);
				error_count++;
			} catch (ParseError e) {
				stderr.printf("error parsing %s: %s\n", file, e.message);
				error_count++;
			}
		}

		/* Abort on parsing errors */
		if (error_count != 0) {
			stderr.printf("error: %u parsing errors occurred. Aborting.\n", error_count);
			return 1;
		}

		/* Print to stdout */
		if (print_to_stdout) {
			foreach (var song in songs) {
				stdout.printf("%s\n%s\n%u\n%s\n", song.title, song.artist, song.capo, song.to_string());
			}
		}

		/* Printing */
		if (print || print_to_file != null) {
			Gtk.PageSetup page_setup;
			PrintOptions print_options;
			Gtk.PrintSettings print_settings;

			page_setup = new Gtk.PageSetup();
			print_options = new PrintOptions();
			print_settings = new Gtk.PrintSettings();

			/* Page setup */
			if (page_size != null) {
				/* Check if paper size exists*/
				var sizes = Gtk.PaperSize.get_paper_sizes(false);
				var exists = false;
				foreach (unowned Gtk.PaperSize size in sizes) {
					if (page_size == size.get_name()) {
						page_setup.set_paper_size_and_default_margins(new Gtk.PaperSize(page_size));

						exists = true;
						break;
					}
				}

				if (exists == false) {
					/* Size doesn't exist, abort */
					stderr.printf("error: paper size %s does not exist. Aborting.\n", page_size);
					return 1;
				}
			}

			if (page_landscape) {
				page_setup.set_orientation(Gtk.PageOrientation.LANDSCAPE);
			}

			/* Margins */
			if (margin_bottom >= 0) {
				page_setup.set_bottom_margin(margin_bottom, Gtk.Unit.MM);
			}
			if (margin_left >= 0) {
				page_setup.set_left_margin(margin_left, Gtk.Unit.MM);
			}
			if (margin_right >= 0) {
				page_setup.set_right_margin(margin_right, Gtk.Unit.MM);
			}
			if (margin_top >= 0) {
				page_setup.set_top_margin(margin_top, Gtk.Unit.MM);
			}

			/* Print options */
			if (column_spacing >= 0) {
				print_options.column_spacing = column_spacing;
			}

			if (font != null) {
				print_options.font = font;
			}

			if (font_size_min != 0) {
				print_options.font_size_min = font_size_min;
			}

			if (font_size_max != 0) {
				print_options.font_size_max = font_size_max;
			}

			/* Print to printer */
			if (print) {
				/* Printer */
				if (printer != null) {
					print_settings.set_printer(printer);
				}

				try {
					var print_operation = create_print_operation(page_setup, print_options, print_settings);

					if (print_dialog) {
						print_operation.run(Gtk.PrintOperationAction.PRINT_DIALOG, new Gtk.Window());
					} else {
						print_operation.run(Gtk.PrintOperationAction.PRINT, null);
					}
				} catch (Error e) {
					stderr.printf("error printing: %s\n", e.message);
					error_count++;
				}
			}

			/* Print to file */
			if (print_to_file != null) {
				try {
					var print_operation = create_print_operation(page_setup, print_options, print_settings);

					print_operation.export_filename = print_to_file;
					print_operation.run(Gtk.PrintOperationAction.EXPORT, null);
				} catch (Error e) {
					stderr.printf("error printing to file: %s\n", e.message);
					error_count++;
				}
			}
		}

		if (error_count != 0) {
			stderr.printf("error: %u printing errors occurred. Aborting.\n", error_count);
			return 1;
		}

		return 0;
	}
}
