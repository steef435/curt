PREFIX ?= /usr

SRC = src/main.vala src/song.vala src/print_options.vala
PKG = gtk+-3.0 json-glib-1.0

TESTS = tests/main.vala tests/song/song.vala

PKG := $(foreach pkg, $(PKG), --pkg $(pkg))
TESTS := $(filter-out src/main.vala, $(SRC)) $(TESTS)

curt: $(SRC) VERSION
	sed 's/.*/const string VERSION = "\0";/g' VERSION > VERSION.vala
	valac -o curt $(SRC) $(PKG) VERSION.vala

install: curt
	install -Dm755 curt $(DESTDIR)$(PREFIX)/bin/curt

test: $(TESTS)
	valac -o test $(TESTS) $(PKG)

clean:
	rm -f curt test VERSION.vala

.PHONY: install clean
