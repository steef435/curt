namespace Tests {
	string get_file_contents(string filename) {
		string contents;
		assert(FileUtils.get_contents(filename, out contents));

		return contents;
	}

	Song new_song(string filename = "tests/song/song") throws ParseError {
		string data = get_file_contents(filename);

		return Song.new_from_data(data);
	}

	void test_to_string() {
		string to_string = get_file_contents("tests/song/song_to_string");
		Song song = new_song();

		assert(to_string == song.to_string());
	}

	/**
	 * Make sure multiline markup doesn't pass
	 */
	void test_multiline_markup() {
		try {
			/* Should fail with ParseError.MARKUP */
			Song song = new_song("tests/song/song_multiline_markup");
		} catch (ParseError e) {
			assert(e is ParseError.MARKUP);
		}
	}

	void song_init() {
		Test.add_func("/song/multiline_markup", test_multiline_markup);
		Test.add_func("/song/to_string", test_to_string);
	}
}
