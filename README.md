# Curt
`curt` prints songs with chords and lyrics, fitting them on a single sheet of paper each. Songs are stored in plain text files. For printing to paper, a two-column layout is used. `curt` will take care of finding the right font size to fit a whole song on one page, in a monospaced font of choice.

## Songs
Songs are stored in plain text files, one song per file. This section describes what formatting options are available.

### Front matter
Every song must start with so-called front matter. This is a [JSON][1] object enclosed by lines consisting of three dashes ("---\n"). Supported properties are:
- `title`: title of the song
- `artist`: artist of the song
- `capo`: number of fret to place capo on, or 0 for no capo

All of these are optional except for `title`. All songs are *required* to have a title property set in the front matter.

Example front matter:

	---
	{
		"title": "D.I.Y. MySpam",
		"artist": "Jayme Gutierrez",
		"capo": 3
	}
	---

### Contents
Front matter is followed by the actual contents of the song. It is important for `curt` to know what lines can be separated. To achieve this, a certain song format is used.

#### Structure

The overall structure of a song is quite simple: a song is made up out of one or multiple paragraphs. A paragraph is a logical set of lines, e.g. a stanza or chorus. On print, they are separated using an empty line.

Paragraphs in turn consist of one or more line sets. A line set is unbreakable: it cannot be split in columns, all the lines in a line set must stick together on print. An example of a line set would be a line of lyrics combined with a line of chords to be played while singing (usually, the line with chords is written directly above the lyrics line). Line sets aren't restricted to sets of two, however, they may consist of one or any larger number of lines.

Of course, creating huge line sets may make it impossible to fit the song on a page. Therefore an understanding of the differences between paragraphs and line sets is essential to a well laid out song.

#### Syntax
##### Line sets
Line sets are separated using empty lines, in other words: one or more lines containing only newline characters (`\n`). These empty lines won't be visible on print. For example, below are three line sets:

	Am               Am
	I am not a musician

	Em   Em
	all I compose is code

	C     C           C          Am
	These chords mean nothing to me
	(no-thing to me)

##### Paragraphs
Paragraphs are separated using a line of three equal signs (`===`). They may be surrounded by additional white lines, those will be ignored. On print, paragraphs will be separated by one empty line. For example, two paragraphs:

	Am               Am
	I am not a musician

	Em   Em
	all I compose is code

	C     C           C          Am
	These chords mean nothing to me
	(no-thing to me)

	===

	I'm putting this guitar away

	I don't know how to play

	Anyway

##### Markup
It is possible to pretty up individual *lines* using extended [Pango Markup][2]. Markup covering multiple lines is not supported. While all tags are supported, altering font size and font family using markup is not recommended, as it breaks the flow of the song and makes predicting relative locations of letters hard or impossible when looking at the song file.

Example of Pango Markup in use:

	<b>I'm putting this guitar away</b>

	<b>I don't know how to play</b>

	<b>Anyway</b>

	===

	<b>Chorus (2x)</b>

###### Extensions
`w`: Underline enclosed non-whitespace blocks (words) only. For example:

	<u>Is this the real code?</u>
	<w>Is this just fantasy?</w>

becomes

	<u>Is this the real code?</u>
	<u>Is</u> <u>this</u> <u>just</u> <u>fantasy?</u>

before getting processed further.

## Project details
`curt` lives at https://gitlab.com/steef435/curt. The code is open source as by the terms of the MIT license. See COPYING for more details.

Discussion, feature requests, bug reports and patches are most appreciated.

[1]: http://json.org/ (JSON specification)
[2]: https://developer.gnome.org/pango/stable/PangoMarkupFormat.html (Pango Markup reference)
